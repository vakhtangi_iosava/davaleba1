fun main(){
    val a = isPalindrome("aba aba a ba")
    print(a)
}

fun isPalindrome(s: String): Boolean {
    val s1 = s.lowercase().replace(" ", "") //ეს სფეისები არ ვიცი ზუსტად უნდა დამეტოვებინა თუ არა, რომ მოვაშორო "ა აა"-ს არ აღიქვამს პალინდრომად
    return s1 == s1.reversed()
}
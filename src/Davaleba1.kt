fun main() {
    val numArray = doubleArrayOf(4.5, 8.65, 89.0, 15.15, -84.45)
    var sum = 0.0
    var count = 0

    for (num in numArray) {
        if (count % 2 == 0){
            sum += num
        }
        count += 1
    }

    val average = sum / numArray.size
    println("The average is: %.2f".format(average))
}